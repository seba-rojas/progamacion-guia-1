#se necesita un programa calcule el area total de la forma A
import math
#se le pide al usuario que ingrese los datos que quiera para el cateto r y la hipotenusa
print("Calcularemos el area de la forma A")
r = float(input("ingrese el valor del cateto R: "))
hi = float(input("ingrese el valor de la hipotenusa: "))
#se hacen los calculos necesarios para saver los valores del cateto faltante
h = hi**2 - r**2
h = math.sqrt(h)
print("el cateto faltante es de valor:", h)
#tambien para saber el area del triangulo de la forma b 
a = (h * r) / 2
print("el area de uno de los triangulos es de:",a)
#y el area del triangulo completo
ar = a * 2
print("el area de los 2 triangulos juntos es de:", ar)
#por ultimo se hacen los calculos para saber el area de la circunferencia
rc = r**2
ac = math.pi * rc
print("el area de la circunferencia es:", ac)
#el cual se divide por 2 y se suman las areas para calcular el area de la figura a
acr = ac / 2
print("el area que necesitamos de la circunferencia es:", acr)
afi = ar + acr
print("el area de la forma A es de:", afi)
